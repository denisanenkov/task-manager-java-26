package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Project;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public class TaskListShowClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Task-list";
    }

    @Override
    public @Nullable String description() {
        return "Get task list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        int count = 1;
        @Nullable final List<Task> taskList = bootstrap.getTaskEndpoint().findAll(bootstrap.getSession());
        if (taskList.isEmpty()) System.out.println("[TASK LIST IS EMPTY]");
        else {
            for (Task task : taskList) {
                System.out.println("Number of task: " + (count++) + "\nName project: " + task.getName() + "\n");
            }
        }
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
