package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskUpdateByIndexClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Task-update-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Update task - list by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NEW NAME TASK: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION TASK: ");
        @NotNull final String description = TerminalUtil.nextLine();
        bootstrap.getTaskEndpoint().updateTaskByIndex(bootstrap.getSession(), index, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
