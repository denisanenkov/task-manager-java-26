package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskFindByIdClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Show-task-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Show task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = bootstrap.getTaskEndpoint().findOneById(bootstrap.getSession(), id);
        if (task == null) throw new IncorrectDataException();
        System.out.println("" +
                "NAME: " + task.getName() +
                ", \nUSER ID: " + task.getUserId()
        );
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
