package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Project;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public class ProjectListShowClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Project-list";
    }

    @Override
    public @Nullable String description() {
        return "Get Project list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        @Nullable final List<Project> projectList = bootstrap.getProjectEndpoint().getListProjects(bootstrap.getSession());
        if (projectList.isEmpty() || projectList.size() == 0) System.out.println("[PROJECT LIST IS EMPTY]");
        else {
            for (Project project : projectList) {
                System.out.println("Number of project: " + (count++) + "\nName project: " + project.getName() +
                        "\nDescription project: " + project.getDescription() + "\n");
            }
        }
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

