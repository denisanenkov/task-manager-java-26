package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class UserUpdateLastNameClientCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-last-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.print("ENTER NEW USER LAST NAME: ");
        @NotNull final String newLastName = TerminalUtil.nextLine();
        bootstrap.getUserEndpoint().updateUserLastName(bootstrap.getSession(), newLastName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
