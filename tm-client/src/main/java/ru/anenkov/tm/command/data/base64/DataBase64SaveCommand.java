package ru.anenkov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

public class DataBase64SaveCommand extends AbstractCommandClient {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        bootstrap.getAdminEndpoint().saveDataBase64(bootstrap.getSession());
        System.out.println();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
