package ru.anenkov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.anenkov.tm.api.service.IEndpointService;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IAuthService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.AuthService;
import ru.anenkov.tm.service.UserService;
import ru.anenkov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.*;

public class BootstrapClient implements IEndpointService {

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();


    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public AdminEndpoint getAdminEndpoint() {
        return adminEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    IUserRepository userRepository = new UserRepository();

    @NotNull
    IUserService userService = new UserService();

    @NotNull
    IAuthService authService = new AuthService(userService);

    @NotNull
    private final Map<String, AbstractCommandClient> commands = new LinkedHashMap<>();

    @Nullable
    Session session = null;

    @Nullable
    public Session getSession() {
        return session;
    }

    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    public void clearSession() {
        session = null;
    }

    public void run(@Nullable final String[] args) throws Exception {
        initCommands();
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    @Nullable
    public Collection<AbstractCommandClient> viewCommands() {
        return commands.values();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return true;
    }

    @SneakyThrows
    private void initCommands() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.anenkov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommandClient>> classes =
                reflections.getSubTypesOf(ru.anenkov.tm.command.AbstractCommandClient.class);
        for (@NotNull final Class<? extends AbstractCommandClient> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@Nullable final AbstractCommandClient command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.name(), command);
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommandClient command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        command.execute();
    }

}
