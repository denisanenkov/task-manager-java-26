package ru.anenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.bootstrap.BootstrapClient;

import javax.persistence.EntityManagerFactory;

public class App {
    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final BootstrapClient bootstrap = new BootstrapClient();
        bootstrap.run(args);
    }
}
