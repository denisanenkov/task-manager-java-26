import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.CreateCategory;
import ru.anenkov.tm.marker.SearchCategory;
import ru.anenkov.tm.marker.AllCategory;

import java.util.ArrayList;
import java.util.List;

@Category(AllCategory.class)
public class SessionEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test
    @Category(CreateCategory.class)
    public void openSessionTest() {
        final Session session = sessionEndpoint.openSession("2", "2");
        Assert.assertNotNull(session);
    }

    @Test
    @Category(CreateCategory.class)
    public void closeSessionTest() {
        final Session session1 = sessionEndpoint.openSession("2", "2");
        int count = sessionEndpoint.allSessions(session1).size();
        Assert.assertNotNull(session1);
        sessionEndpoint.closeSessionAll(session1);
        sessionEndpoint.closeSession(session1);
    }

    @Test
    @Category(SearchCategory.class)
    public void allSessions() {
        final Session session = sessionEndpoint.openSession("2", "2");
        int count = sessionEndpoint.allSessions(session).size();
        List<Session> sessions = new ArrayList<>();
        Assert.assertTrue(sessions.isEmpty());
        sessions = sessionEndpoint.allSessions(session);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(count, sessions.size());
    }

}
