package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IProjectEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public ProjectEndpoint() {
        super(null);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void createProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void addProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    public void removeProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final ProjectDTO project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<ProjectDTO> findAllProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().toProjectDTOList(serviceLocator.getProjectService().findAllEntities(session.getUserId()));
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void clearProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().toProjectDTO
                (serviceLocator.getProjectService().findOneByIndexEntity(session.getUserId(), index));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByNameProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().toProjectDTO
                (serviceLocator.getProjectService().findOneByNameEntity(session.getUserId(), name));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public ProjectDTO findOneByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().toProjectDTO
                (serviceLocator.getProjectService().findOneByIdEntity(session.getUserId(), id));
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public void removeOneByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void removeOneByNameProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void removeOneByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void updateByIdProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") final String id,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void updateByIndexProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index") final Integer index,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void loadProject(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "tasks") final List project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().load(project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<ProjectDTO> getListProjects(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        List<Project> projects = serviceLocator.getProjectService().findAllEntities(session.getUserId());
        return serviceLocator.getProjectService().toProjectDTOList(projects);
    }

}
