package ru.anenkov.tm.endpoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.ISessionEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.api.service.ISessionService;
import ru.anenkov.tm.dto.result.Fail;
import ru.anenkov.tm.dto.result.Result;
import ru.anenkov.tm.dto.result.Success;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import java.util.List;

@WebService
@JsonIgnoreProperties
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public SessionEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public SessionDTO openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.toSessionDTO(sessionService.open(login, password));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        try {
            super.serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        try {
            super.serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<SessionDTO> allSessions(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        super.serviceLocator.getSessionService().validate(session);
        List<SessionDTO> sessions = super.serviceLocator.getSessionService().getListSession(session);
        System.out.println(sessions);
        return sessions;
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void clearAllSessions() {
        serviceLocator.getSessionService().closeAll();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SessionDTO findSessionById(
            @Nullable @WebParam(name = "sessionId", partName = "sessionId") final String sessionId
    ) {
        SessionDTO sessionDTO = serviceLocator.getSessionService().toSessionDTO
                (serviceLocator.getSessionService().findSessionById(sessionId));
        return sessionDTO;
    }

}
