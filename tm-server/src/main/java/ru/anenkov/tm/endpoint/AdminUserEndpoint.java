package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.api.endpoint.IAdminUserEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint extends AdminEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public AdminUserEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void lockUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void deleteUserByLogin(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().deleteUserByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "user") UserDTO user
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().removeUser
                (serviceLocator.getUserService().toUser(user));
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByIdUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByLoginUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    @Override
    @NotNull
    @SneakyThrows
    public void removeByEmailUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "email") String email
    ) {
        serviceLocator.getSessionService().validate(
                serviceLocator.getSessionService().toSession(session), Role.ADMIN);
        serviceLocator.getUserService().removeByEmail(email);
    }

}
