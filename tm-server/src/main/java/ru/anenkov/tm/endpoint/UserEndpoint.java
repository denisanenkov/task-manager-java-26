package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IUserEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public UserEndpoint() {
        super(null);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<UserDTO> findAllUser(
            @NotNull @WebParam(name = "session") SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().toUserDTOList
                (serviceLocator.getUserService().findAllEntities());
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void createUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().create(login, password, Role.USER);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByIdUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().toUserDTO
                (serviceLocator.getUserService().findByIdEntity(id));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByLoginUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().toUserDTO
                (serviceLocator.getUserService().findByLoginEntity(login));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByEmailUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().toUserDTO
                (serviceLocator.getUserService().findByLoginEntity(login));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updateUserFirstName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newFirstName") String newFirstName
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserFirstName(session.getUserId(), newFirstName);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updateUserMiddleName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newMiddleName") String newMiddleName
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserMiddleName(session.getUserId(), newMiddleName);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updateUserLastName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newLastName") String newLastName
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserLastName(session.getUserId(), newLastName);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updateUserEmail(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newEmail") String newEmail
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserEmail(session.getUserId(), newEmail);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updatePasswordUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newPassword") String newPassword
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updatePassword(session.getUserId(), newPassword);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<UserDTO> getListUser(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().toUserDTOList
                (serviceLocator.getUserService().findAllEntities());
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO showUserProfile(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return (serviceLocator.getAuthService().showUserProfile());
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    public void checkRoleUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "roles") final Role[] roles
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getAuthService().checkRole(roles);
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    public UserDTO findById(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "roles") final Role[] roles
    ) {
        serviceLocator.getSessionService().validate(session);
        UserDTO userDTO = serviceLocator.getUserService().toUserDTO
                (serviceLocator.getUserService().findByIdEntity(session.getUserId()));
        return userDTO;
    }

}