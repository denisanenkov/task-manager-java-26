package ru.anenkov.tm.endpoint;

import ru.anenkov.tm.api.service.IServiceLocator;

import javax.validation.constraints.NotNull;

public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    protected AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
