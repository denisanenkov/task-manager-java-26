package ru.anenkov.tm.dto.entitiesDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractEntityDTO {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId = "";

    @Override
    public String toString() {
        return "\nProject:" +
                "\nname = '" + name + '\'' +
                ", \ndescription ='" + description + '\'' +
                ", \nuserId ='" + userId;
    }

    @Nullable
    public ProjectDTO(@Nullable final Project project) {
        if (project == null) return;
        assert project.getName() != null;
        name = project.getName();
        assert project.getDescription() != null;
        description = project.getDescription();
        if (project.getUser() != null) userId = project.getUser().getId();
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Nullable
    public static ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(@Nullable final Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            result.add(new ProjectDTO(project));
        }
        return result;
    }

}