package ru.anenkov.tm.dto.entitiesDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Session;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    private Long timestamp;

    private String userId;

    private String signature;

    public SessionDTO() {
    }

    public SessionDTO(String userId) {
        this.userId = userId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "\nSession:\n" +
                "\ntimestamp =" + timestamp +
                ", \nuserId ='" + userId + '\'' +
                ", \nsignature ='" + signature + '\'' +
                "\n";
    }

    @Nullable
    public SessionDTO(@Nullable final Session session) {
        if (session == null) return;
        timestamp = session.getTimestamp();
        assert session.getTimestamp() != null;
        userId = session.getUser().getId();
        signature = session.getSignature();
        assert session.getSignature() != null;
        if (session.getUser() != null) userId = session.getUser().getId();
    }

    @Nullable
    public static SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        return new SessionDTO(session);
    }

    @NotNull
    public static List<SessionDTO> toDTO(@Nullable final Collection<Session> sessions) {
        if (sessions == null || sessions.isEmpty()) return Collections.emptyList();
        @NotNull final List<SessionDTO> result = new ArrayList<>();
        for (@Nullable final Session session : sessions) {
            if (session == null) continue;
            result.add(new SessionDTO(session));
        }
        return result;
    }

}
