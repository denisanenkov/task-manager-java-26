package ru.anenkov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.*;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.repository.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;

import javax.xml.ws.Endpoint;
import java.io.IOException;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, userService, projectService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final IDataService dataService = new DataService(domainService);

    @NotNull
    private final ISqlSessionService sqlSessionService = new EntityManagerService();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final CalcEndpoint calcEndpoint = new CalcEndpoint();

    public Bootstrap() throws IOException {
    }

    private void initEndpoint() {
        registryEndpoint(adminEndpoint);
        registryEndpoint(sessionEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
        registryEndpoint(adminUserEndpoint);
        registryEndpoint(calcEndpoint);
    }

    private void registryEndpoint(final Object endpoint) {
        if (endpoint == null) return;
        final String host = DataConst.SERVER_HOST;
        final String port = DataConst.SERVER_PORT;
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(wsdl, endpoint);
        System.out.println(wsdl);
    }

    public void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("2", "2", "den@mail.ru");
    }

    public void run(@Nullable final String[] args) throws Exception {
        initEndpoint();
        //initUsers();
        System.out.println("\t\t***SERVERS*STARTED***");
        if (parseArgs(args)) System.exit(0);
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return true;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    public IDataService getDataService() {
        return dataService;
    }

    @NotNull
    public ISqlSessionService getSqlSessionService() {
        return sqlSessionService;
    }

}