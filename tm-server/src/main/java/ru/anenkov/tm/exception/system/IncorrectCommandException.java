package ru.anenkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.exception.AbstractException;

public class IncorrectCommandException extends AbstractException {

    public IncorrectCommandException(@NotNull final String command) {
        super("Error! ``" + command + "`` is not a command! Re-enter command!");
    }

    public IncorrectCommandException(Throwable cause) {
        super(cause);
    }

    public IncorrectCommandException() {
    }

}