package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty!");
    }

}
