package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
@Entity
@Table(name = "app_project")
public class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks;

    public Project(
            final @Nullable String name,
            final @Nullable User user
    ) {
        this.name = name;
        this.user = user;
    }

    public Project(
            final @Nullable String name,
            final @Nullable String description,
            final @Nullable User user
    ) {
        this.name = name;
        this.description = description;
        this.user = user;
    }

    @Override
    public String toString() {
        return "\nProject:" +
                "\nname = '" + name + '\'' +
                ", \ndescription ='" + description + '\'' +
                ", \nuserId ='" + user.getId() + "\n";
    }

}
