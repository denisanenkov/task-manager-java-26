package ru.anenkov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.UserService;

import java.util.UUID;

public final class SignatureUtil {

    public static String sign(
            final Object value,
            final String salt,
            final Integer cycle
    ) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }

    public static String sign(
            String value,
            String salt,
            Integer cycle
    ) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.MD5(salt + result + salt);
        }
        return result;
    }

    public static void main(String[] args) throws JsonProcessingException {
        UserService userService = new UserService();
        SessionDTO sessions = new SessionDTO();
        sessions.setId(UUID.randomUUID().toString());
        sessions.setUserId(UUID.randomUUID().toString());
        sessions.setTimestamp(System.currentTimeMillis());
        Session newSession = new Session(userService.findByIdEntity(sessions.getId()));
        newSession.setSignature(sign(sessions, "MEGA", 2234));
    }

}
