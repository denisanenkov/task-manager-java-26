package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.result.Result;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.validation.constraints.NotNull;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    SessionDTO openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @NotNull
    @WebMethod
    Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @NotNull
    @WebMethod
    Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @NotNull
    @WebMethod
    List<SessionDTO> allSessions(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @NotNull
    @WebMethod
    void clearAllSessions();

    @Nullable
    @WebMethod
    SessionDTO findSessionById(
            @Nullable @WebParam(name = "sessionId", partName = "sessionId") final String sessionId
    );

}
