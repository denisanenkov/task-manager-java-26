package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @Nullable List<UserDTO> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    void createUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login", partName = "login") final String login,
            @NotNull @WebParam(name = "password", partName = "password") final String password
    );

    @WebMethod
    @Nullable UserDTO findByIdUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @Nullable UserDTO findByLoginUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    @Nullable UserDTO findByEmailUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    );


    @WebMethod
    @Nullable void updateUserFirstName(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newFirstName", partName = "newFirstName") final String newFirstName
    );

    @WebMethod
    @Nullable void updateUserMiddleName(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newMiddleName", partName = "newMiddleName") final String newMiddleName
    );

    @WebMethod
    @Nullable void updateUserLastName(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newLastName", partName = "newLastName") final String newLastName
    );

    @WebMethod
    @Nullable void updateUserEmail(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newEmail", partName = "newEmail") final String newEmail
    );

    @WebMethod
    @Nullable void updatePasswordUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") final String newPassword
    );

    @WebMethod
    @Nullable List<UserDTO> getListUser(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    UserDTO showUserProfile(
            @NotNull @WebParam(name = "session") final SessionDTO session
    );

}
