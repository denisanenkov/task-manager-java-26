package ru.anenkov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @SneakyThrows
    void createProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void addProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "session", partName = "session") final Project project
    );

    @WebMethod
    @SneakyThrows
    void removeProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task", partName = "task") final ProjectDTO project
    );

    @WebMethod
    @SneakyThrows
    List<ProjectDTO> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void clearProjects(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    ProjectDTO findOneByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    ProjectDTO findOneByNameProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    ProjectDTO findOneByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneByNameProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    void updateByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void updateByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );


    @WebMethod
    void loadProject(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task", partName = "task") final List project
    );

    @WebMethod
    List<ProjectDTO> getListProjects(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

}
