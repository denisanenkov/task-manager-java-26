package ru.anenkov.tm.api.service;

import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import javax.validation.constraints.NotNull;

public interface IAuthService {

    @NotNull
    UserDTO showUserProfile();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRole(Role[] roles);

    void logout();

    void registry(String login, String password, String email);

    void updatePassword(String newPassword);

    void updateUserFirstName(String newFirstName);

    void updateUserMiddleName(String newMiddleName);

    void updateUserLastName(String newLastName);

    void updateUserEmail(String newEmail);

}
