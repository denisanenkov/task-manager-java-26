package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.AbstractEntityDTO;
import ru.anenkov.tm.entity.AbstractEntity;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface IService<T extends AbstractEntity> {

    void load(@Nullable T... t);

    @Nullable
    List<T> getList();

    @NotNull
    T merge(@Nullable T element);

    List<T> merge(@Nullable Collection<T> elements);

    List<T> merge(@Nullable T... elements);

}
