package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    List<Task> getList();

    @Nullable
    List<TaskDTO> getListDTOs(String userId);

    @Nullable
    List<Task> getListEntities(String userId);

    @Nullable
    TaskDTO toTaskDTO(@Nullable final Task task);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId);

    @Nullable
    List<TaskDTO> toTaskDTOList(@Nullable final List<Task> taskList);

    @Nullable
    TaskDTO findOneByIdDTO(@Nullable String userId, @NotNull String id);

    @Nullable
    Task findOneByIdEntity(@Nullable String userId, @NotNull String id);

    @Nullable
    Task findOneByNameEntity(@NotNull String userId, @NotNull String name);

    @Nullable
    TaskDTO findOneByNameDTO(@Nullable String userId, @NotNull String name);

    @Nullable
    Task findOneByIndexEntity(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByIndexDTO(@Nullable String userId, @NotNull Integer index);

    @Nullable
    Task toTask(@Nullable String userId, @Nullable TaskDTO taskDTO);

    @Nullable
    List<Task> toTaskList(@Nullable String userId, @Nullable List<TaskDTO> taskDTOList);

    void load(@Nullable Task... tasks);

    void clear(@Nullable String userId);

    void load(@Nullable List<Task> tasks);

    void add(@Nullable String userId, @NotNull TaskDTO task);

    void remove(@Nullable String userId, @NotNull Task task);

    void removeOneById(@Nullable String userId, @NotNull String id);

    void removeOneByName(@Nullable String userId, @NotNull String name);

    void removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    void create(@Nullable String userId, @NotNull String name);

    void create(@Nullable String userId, @NotNull String name, @NotNull String description);

    void updateTaskById(@Nullable String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    void updateTaskByName(@Nullable String userId, @NotNull String oldName, @NotNull String newName, @NotNull String description);

    void updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String newName, @Nullable String description);

}
