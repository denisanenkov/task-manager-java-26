package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.AbstractEntityDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.AbstractEntity;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface IRepository<T extends AbstractEntity> {

    @NotNull
    void clear();

    @NotNull
    List<T> merge(@NotNull Collection<T> t);

    @NotNull
    T merge(@NotNull T t);

    @NotNull
    List<T> merge(@NotNull T... t);

    @NotNull
    void load(@NotNull Collection<T> t);

    @NotNull
    void load(@NotNull T... t);

    @NotNull
    void load(@NotNull T t);

    @Nullable
    List<T> getList();

    @NotNull
    void remove(T t);

}
