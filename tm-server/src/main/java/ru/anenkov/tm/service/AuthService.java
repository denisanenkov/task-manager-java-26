package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IAuthService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Nullable
    @SneakyThrows
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    @SneakyThrows
    public void checkRole(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final String userId = getUserId();
        @Nullable final UserDTO user = userService.findByIdDTO(userId);
        assert user != null;
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@Nullable final Role item : roles) {
            if (role.equals(item)) {
                return;
            }
            throw new AccessDeniedException();
        }
    }

    @Override
    @SneakyThrows
    public void logout() {
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userId = null;
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (email == null || email.isEmpty()) return;
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.create(login, password, email);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updatePassword(@NotNull final String newPassword) {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.updateUserFirstName(userId, newPassword);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO showUserProfile() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            @Nullable final UserDTO userDTO = userService.findByIdDTO(userId);
            userRepository.commitTransaction();
            return userDTO;
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateUserFirstName(@Nullable final String newFirstName) {
        if (newFirstName == null || newFirstName.isEmpty()) return;
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.updateUserFirstName(userId, newFirstName);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateUserMiddleName(@Nullable final String newMiddleName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newMiddleName == null || newMiddleName.isEmpty()) return;
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.updateUserMiddleName(userId, newMiddleName);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateUserLastName(@Nullable final String newLastName) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newLastName == null || newLastName.isEmpty()) return;
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.updateUserLastName(userId, newLastName);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

    @Override
    @SneakyThrows
    public void updateUserEmail(@Nullable final String newEmail) {
        if (!isAuth()) throw new AccessDeniedException();
        if (newEmail == null || newEmail.isEmpty()) return;
        @Nullable final IUserRepository userRepository = new UserRepository();
        try {
            userRepository.startTransaction();
            userService.updateUserFirstName(userId, newEmail);
            userRepository.commitTransaction();
        } catch (Exception ex) {
            userRepository.rollbackTransaction();
            throw ex;
        } finally {
            userRepository.closeTransaction();
        }
    }

}
