package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IProjectEndpoint;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.HashUtil;
import ru.anenkov.tm.util.TerminalUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public final class EntityManagerService implements ISqlSessionService {

    @Nullable
    EntityManager entityManager;

    public EntityManagerService() throws IOException {
        entityManager = getEntityManager();
    }

    @Nullable
    private static EntityManagerFactory entityManagerFactory;

    public static EntityManager getEntityManager() throws IOException {
        init();
        return entityManagerFactory.createEntityManager();
    }

    public static void init() throws IOException {
        entityManagerFactory = factory();
    }

    private static EntityManagerFactory factory() throws IOException {
        @Nullable final Map<String, String> settings = new HashMap<>();
        @NotNull final Properties properties = new Properties();
        properties.load(new FileInputStream(new File(DataConst.PROPERTIES_PATH)));
        settings.put(Environment.DRIVER, properties.getProperty("jdbc.driver"));
        settings.put(Environment.URL, properties.getProperty("jdbc.url"));
        settings.put(Environment.USER, properties.getProperty("jdbc.username"));
        settings.put(Environment.PASS, properties.getProperty("jdbc.password"));
        settings.put(Environment.DIALECT, properties.getProperty("jdbc.dialect"));
        settings.put(Environment.HBM2DDL_AUTO, properties.getProperty("jdbc.ddlauto"));
        settings.put(Environment.SHOW_SQL, properties.getProperty("jdbc.showsql"));
        settings.put(Environment.JDBC_TIME_ZONE, properties.getProperty("jdbc.timezone"));
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
